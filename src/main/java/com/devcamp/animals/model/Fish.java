package com.devcamp.animals.model;

public class Fish extends Animal {
    private int size;
    private boolean canEat;

    public Fish() {
        super();
    }

    public Fish(int age, String gender) {
        super(age, gender);
    }

    public Fish(int size, boolean canEat) {
        super();
        this.size = size;
        this.canEat = canEat;
    }

    public Fish(int age, String gender, int size, boolean canEat) {
        super(age, gender);
        this.size = size;
        this.canEat = canEat;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

    @Override
    public void isMammal() {
        System.out.println("Fish isn't mammal!");
    }

    public void swim() {
        System.out.println("Fish swimming...");
    }

    public void mate() {
        System.out.println("Fish mating...");
    }
}
