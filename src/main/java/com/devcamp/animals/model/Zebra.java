package com.devcamp.animals.model;

public class Zebra extends Animal {
    private boolean is_wild;

    public Zebra() {
        super();
    }

    public Zebra(boolean is_wild) {
        super();
        this.is_wild = is_wild;
    }

    public Zebra(int age, String gender, boolean is_wild) {
        super(age, gender);
        this.is_wild = is_wild;
    }

    @Override
    public void isMammal() {
        System.out.println("Zebra is mammal");
    }

    public void run() {
        System.out.println("Zebra running...");
    }

    public void mate() {
        System.out.println("Zebra mating...");
    }

    public boolean isIs_wild() {
        return is_wild;
    }

    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
}
