package com.devcamp.animals.controller;

import com.devcamp.animals.model.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@RestController
public class AnimalController {
    @GetMapping("/listAnimal")
    public ArrayList<Animal> listAnimal() {
        ArrayList<Animal> lstAnimal = new ArrayList<Animal>();

        Animal duck = new Duck();
        duck.setAge(2);
        duck.setGender("male");
        ((Duck) duck).setBeakColor("yellow");
        duck.mate();
        duck.isMammal();
        ((Duck) duck).swim();
        ((Duck) duck).quack();

        Duck duck2 = new Duck(3, "female", "white");
        duck2.mate();
        duck2.swim();
        duck2.quack();

        Fish fish = new Fish(1, "male", 15 , true);
        fish.isMammal();
        fish.mate();
        fish.swim();
        
        Zebra zebra = new Zebra(5, "male", true);
        zebra.isMammal();
        zebra.mate();
        zebra.run();

        lstAnimal.add(duck);
        lstAnimal.add(duck2);
        lstAnimal.add(fish);
        lstAnimal.add(zebra);

        return lstAnimal;
    }
}
