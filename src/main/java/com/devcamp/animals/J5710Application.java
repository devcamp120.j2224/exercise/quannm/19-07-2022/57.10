package com.devcamp.animals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J5710Application {

	public static void main(String[] args) {
		SpringApplication.run(J5710Application.class, args);
	}

}
